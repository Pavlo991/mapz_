﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class IdleState : State
{
    public override void Execute(PlayerScript character)
    {
        character.State = CharState.Idle;
    }
}
