﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Status : MonoBehaviour
{
    public Text textForMoney;
    public Text textForDiamond;
    private PlayerScript character;

    int money;
    int diamond;

    private void Start()
    {
        textForMoney.text = character.startGame.state.Money.ToString();
        textForDiamond.text = character.startGame.state.Diamond.ToString();
    }

    private void Awake()
    {
        character = FindObjectOfType<PlayerScript>();
    }
    public void Refresh()
    {
        textForMoney.text = character.startGame.state.Money.ToString();
        textForDiamond.text = character.startGame.state.Diamond.ToString();
    }
}
