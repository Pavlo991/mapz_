﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LevelBarFirst : MonoBehaviour, LevelStategy
{
    private Transform[] parts = new Transform[4];
    private PlayerScript character;

    private void Awake()
    {
        character = FindObjectOfType<PlayerScript>();

        for (int i = 0; i < parts.Length; i++)
        {
            parts[i] = transform.GetChild(i);
            parts[i].gameObject.SetActive(true);
        }
        Debug.Log("Awake: " + parts.Length + "  " + gameObject);
    }
    public void LoadLevelStr()
    {
        //Debug.Log("Load: " + parts.Length + "  " + this);
        for (int i = 0; i < parts.Length; i++)
            parts[i].gameObject.SetActive(true);
    }
}

