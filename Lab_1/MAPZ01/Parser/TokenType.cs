﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    public enum TokenType
    {
        NUMBER,
        WORD,
        TEXT,

        //keyword
        PRINT,
        IF,
        ELSE,
        WHILE,
        FOR,
        DO,
        BRAKE,
        CONTINUE,

        GO,
        FIND,
        SETINPUT,
        CLICK,
        QUITE,

        PLUS,
        MINUS,
        MULTIPLICATION,
        DIVISION,
        EQ,
        EQEQ,
        EXCL, // !
        EXCLEQ, // !=
        LESS,
        LESSEQ, // <=
        GREATER, 
        GREATEREQ, // >=

        BAR, // |
        BARBAR,
        AMP, //&
        AMPAMP,

        LPAREN, // (
        RPAREN, // )
        LBRACE, // {
        RBRACE, // }
        DOTECOMMA, // ;
        DOT, // ;

        EOF
    }
}
