﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class Token
    {
        private TokenType type;
        private string text;

        public Token()
        {
        }
        public Token(TokenType type, string text)
        {
            this.type = type;
            this.text = text;
        }

        public void setType(TokenType type)
        {
            this.type = type;
        }
        public TokenType getType()
        {
            return this.type;
        }
        public void setText(string text)
        {
            this.text = text;
        }
        public string getText()
        {
            return this.text;
        }

        public override string ToString()
        {
            return type + " " + text;
        }
    }
}
