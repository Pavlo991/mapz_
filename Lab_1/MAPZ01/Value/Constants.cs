﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class Constants
    {
        private static Dictionary<string, Value> constants = new Dictionary<string, Value>()
        {
            { "PI", new NumberValue(Math.PI) },
            { "pi", new NumberValue(Math.PI) },
            { "E", new NumberValue(Math.E) },
            { "e", new NumberValue(Math.E) }
        };

        public static bool isExists(string key)
        {
            return constants.ContainsKey(key);
        }

        public static Value get(string key)
        {
            if (!isExists(key)) return new NumberValue(0);
            return constants[key];
        }

        public static void set(string key, Value value)
        {
            if (!constants.ContainsKey(key))
            {
                constants.Add(key, value);
            }
            else constants[key] = value;
        }
    }
}
