﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class StringValue: Value
    {
        private string value;

        public StringValue(string value)
        {
            this.value = value;
        }
      
        public override string ToString()
        {
            return value;
        }

        double Value.asDouble()
        {
            try
            {
                return Convert.ToDouble(value);
            }
            catch (FormatException)
            {
                return 0;
            }
        }

        string Value.asString()
        {
            return value;
        }
    }
}
