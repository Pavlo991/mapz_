﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class DoWhileStatement: Statement
    {
        private Expression condition;
        private Statement statement;
        private List<BlockStatement> listOfBlock;


        public DoWhileStatement(Expression condition, Statement statement)
        {
            this.condition = condition;
            this.statement = statement;
            listOfBlock = new List<BlockStatement>();

        }

        public string execute()
        {
            StringBuilder result = new StringBuilder();

            do
            {
                try
                {
                    listOfBlock.Add(statement as BlockStatement);
                    result.Append(statement.execute());
                }
                catch (BreakStatement)
                {
                    break;
                }
                catch (ContinueStatement)
                {
                    continue; //необовязкове (ми все одно перейдемо)
                }
            } while (condition.eval().asDouble() != 0);

            return result.ToString();
        }

        public List<Statement> getListOfStatement()
        {
            List<Statement> listOfStatement = new List<Statement>();
            foreach (BlockStatement blocks in listOfBlock)
            {
                foreach (Statement statament in blocks.getList())
                {
                    listOfStatement.Add(statament);
                }
            }
            return listOfStatement;
        }

        public override string ToString()
        {
            return "do " + statement + "while " + condition;
        }
    }
}
