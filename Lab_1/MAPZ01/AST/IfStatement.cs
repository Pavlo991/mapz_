﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class IfStatement : Statement
    {
        private Expression expr;
        Statement ifStatement, elseStatement;
        private BlockStatement blockStatement;
        private List<Statement> listOfStatement;

        public IfStatement(Expression expr, Statement ifStatement, Statement elseStatement)
        {
            this.expr = expr;
            this.ifStatement = ifStatement;
            this.elseStatement = elseStatement;
            listOfStatement = new List<Statement>();
            blockStatement = new BlockStatement();
        }

        public string execute()
        {
            double result = expr.eval().asDouble();
            string str = "";
            if (result != 0)
            {
                blockStatement = ifStatement as BlockStatement;
                str = ifStatement.execute();
            }
            else if (elseStatement != null)
            {
                blockStatement = elseStatement as BlockStatement;
                str = elseStatement.execute();
            }
            return str;
        }

        public List<Statement> getListOfStatement()
        {
            foreach(Statement statements in blockStatement.getList())
            {
                listOfStatement.Add(statements);
            }
            return listOfStatement;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append("if ").Append(expr).Append(" ").Append(ifStatement);
            if(elseStatement != null)
            {
                str.Append("\nelse ").Append(elseStatement);
            }
            return str.ToString();
        }
    }
}
