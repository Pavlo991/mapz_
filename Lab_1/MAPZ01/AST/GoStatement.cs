﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class GoStatement: Statement
    {
        private Expression link;

        public GoStatement(Expression link)
        {
            this.link = link;
        }

        public string execute()
        {
            StringBuilder str = new StringBuilder();
            str.Append(link);
            return str.ToString();
        }

        public override string ToString()
        {
            return link.ToString();
        }
    }
}
