﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class UnaryExpression: Expression
    {
        private char operation;
        private Expression expr1;

        public UnaryExpression(char operation, Expression expr1)
        {
            this.operation = operation;
            this.expr1 = expr1;
        }

        public Value eval()
        {
                switch (operation)
                {
                    case '-': return new NumberValue(-expr1.eval().asDouble());
                    case '+':
                    default:
                        return new NumberValue(expr1.eval().asDouble());
                }
        }

        public override string ToString()
        {
            return String.Format("{0}{1}", operation, expr1);
        }
    }
}
