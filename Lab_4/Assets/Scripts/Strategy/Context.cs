﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Context : MonoBehaviour
{
        private LevelStategy _strategy;

        // Constructor

        public Context(LevelStategy strategy)
        {
            this._strategy = strategy;
        }

        public void ContextLoadLevel()
        {
            _strategy.LoadLevelStr();
        }
    }

