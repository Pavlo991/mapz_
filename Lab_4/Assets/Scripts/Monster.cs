﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : Unit
{
    protected virtual void Awake() { }
    protected virtual void Start() { }
    protected virtual void Update() { }
    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        Unit unit = collision.GetComponent<Unit>();

        if (unit && unit is PlayerScript)
        {
            unit.ReceiveDamage();
        }
        //else Debug.Log("Error...\nDon't touch the player");


        Bullet bullet = collision.GetComponent<Bullet>();

        if (bullet)
        {
            ReceiveDamage();
        }
        //else Debug.Log("Error...\nDon't touch the bullet
    }

}
