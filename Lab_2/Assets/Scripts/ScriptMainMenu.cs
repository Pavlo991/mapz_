﻿using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class ScriptMainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        GlobalVariables.isNewGame = true;
        Application.LoadLevel("Game");
    }
    public void PlaySavedGame()
    {
        GlobalVariables.isNewGame = false;
        Application.LoadLevel("Game");
    }
}
